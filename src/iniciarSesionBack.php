<?php
session_start();
require_once("DbManager.php");

$usuario = urldecode($_GET["name"]);
$contraseña = urldecode($_GET["contrasenia"]);
$conector = DbManager::crearConector();

$stmt = $conector->prepare("select contrasenia from usuario where usuario=?");
$stmt->bind_param("s", $usuario);
$stmt->execute();
$stmt->store_result(); // Almacenar el resultado para poder usar num_rows()
$num_rows = $stmt->num_rows;
$stmt->bind_result($passwordHash);
$stmt->fetch();
$stmt->close();
$conector->close();

if($num_rows == 0){
    $mensaje = "¡Usuario no encontrado!";
    $_SESSION['mensaje_alerta'] = $mensaje;
    header("Location: iniciarSesion.php");
} else if (password_verify($contraseña, $passwordHash)) {
    
    $_SESSION["usuario"] = $usuario;
    header("Location: index.php");

} else {
     $mensaje = "¡Contraseña incorrecta!";
     $_SESSION['mensaje_alerta'] = $mensaje;
     header("Location: iniciarSesion.php");
}