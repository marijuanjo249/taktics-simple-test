<?php
require_once("DbManager.php");
$conector = DbManager::crearConector();
// Verificar si se ha enviado la acción de eliminar
if (isset($_POST['opcionesSeleccionadas'])) {
    // Recuperar los IDs de los elementos que se desean eliminar
    $idsEliminar = $_POST['opcionesSeleccionadas'];
    foreach ($idsElimiar as $idEliminar) {
        $stmt = $conector->prepare("delete from balance where idBalance=?");
        $stmt->bind_param("s", $idEliminar['$idBalance']);
        $stmt->execute();
        $stmt->close();
    }
    $mensaje = "¡Eliminación realizada con éxito!";
    $_SESSION['mensaje_alerta'] = $mensaje;
    header("Location: balance.php");

    
    // Procesar los IDs de eliminación, por ejemplo, realizar una consulta SQL para eliminar los registros correspondientes
    // Ejemplo:
    // DELETE FROM tabla WHERE id IN ($idsEliminar)
    // Esto es solo un ejemplo, asegúrate de sanitizar y validar los datos antes de usarlos en una consulta SQL
}

// Verificar si se ha enviado la acción de modificar
if (isset($_GET['m'])) {
    // Recuperar el ID del elemento que se desea modificar
    $idModificar = $_GET['m'];
    $stmt = $conector->prepare("select * from balance where idBalance=?");
    $stmt->bind_param("s", $idModificar);
    $stmt->execute();
    $stmt->bind_result($idBalance, $tipo, $amount, $name, $category, $date, $iduser);
    $stmt->fetch();
    $stmt->close();
    $conector->close();
    
    // Procesar el ID de modificación, por ejemplo, realizar una consulta SQL para actualizar el registro correspondiente
    // Ejemplo:
    // UPDATE tabla SET campo = nuevo_valor WHERE id = $idModificar
    // Esto es solo un ejemplo, asegúrate de sanitizar y validar los datos antes de usarlos en una consulta SQL
}




?>
<!doctype html>
<html class=no-js lang="">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
        <meta charset=utf-8>
        <meta name=description content="">
        <meta name="viewport"
                content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <title>Balance</title>
        <link rel="shortcut icon" href=favicon.ico>
        <link rel=stylesheet href=vendor.css>
        <link rel=stylesheet href=style.css>
        <link rel=stylesheet type=text/css href=css/layerslider.css>

</head>

<body>
        <section class="map_wrapper clearfix">
                <div id=map-section></div>
                <div class=container>
                        <div class=row>
                                <div class=contact_form>
                                        <h2 class=heading>Update Details <span></span></h2>

                                        <form action="procesBack.php" method="get" name=contact
                                                class="formcontact clearfix">  
                                                
                                                <div class=form-group style="color: grey;"> Amount<input type=text class=form-control name=import
                                                                placeholder=Amount value="<?php echo $amount ?>" required=""
                                                                data-parsley-required-message="porfavor inserte nombre"></input>
                                                </div>                                                

                                                <div class=form-group style="color: grey;"> Name<input type=text class=form-control name=name
                                                                placeholder=Name value="<?php echo $name ?>" required=""
                                                                data-parsley-required-message="porfavor inserte nombre"></input>
                                                </div>                                                

                                                <div class=form-group style="color: grey;"> Category<input type=text class=form-control name=category
                                                                placeholder=Category value="<?php echo $category ?>" required=""
                                                                data-parsley-required-message="porfavor inserte nombre"></input>
                                                </div>  
                                                <div class=form-group style="color: grey; display:none;"> id<input type=text class=form-control name=id
                                                                placeholder=Nombre value="<?php echo $idBalance ?>" required=""
                                                                data-parsley-required-message="porfavor inserte nombre"></input>
                                                </div>                                               
                                                <button type=submit class="btn btn-white" id=send>Actualizar</button>
                                        </form>
                                </div>
                        </div>
                </div>
        </section>
</body>