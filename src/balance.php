<?php
session_start();
require_once("DbManager.php");

if (isset($_SESSION["usuario"])) {
    $usuario = $_SESSION["usuario"];
} else {
    header("Location: iniciarSesion.php");
}

// Función para eliminar elementos

// Consulta para obtener la clasificación de equipos
$conector = DbManager::crearConector();
$stmt = $conector->prepare("SELECT balance.date, balance.tipo, balance.amount
FROM balance
JOIN usuario ON usuario.usuario = ?
AND usuario.idUsuario = balance.iduser ORDER BY balance.date ASC");
$stmt->bind_param("s", $usuario);
$stmt->execute();
$result = $stmt->get_result();

$balances = array();
$totalBalance = 0;
$income = 0;
$expense = 0;

while ($balance = $result->fetch_assoc()) {
    $fecha = $balance['date'];
    $tipo = $balance['tipo'];
    $monto = $balance['amount'];

    if (!isset($balances[$fecha])) {
        $balances[$fecha] = array('total' => 0);
    }

    if ($tipo == 0) {
        // Es un ingreso
        $balances[$fecha]['total'] += $monto;
        $income += $monto;
    } else {
        // Es un egreso
        $balances[$fecha]['total'] -= $monto;
        $expense += $monto;
    }

    $totalBalance += $tipo == 0 ? $monto : -$monto;
    $balances[$fecha]['total'] = $totalBalance;
}

$data = array();
$dataWeek = array();
$dataMonth = array();
$dataYear = array();

$labels = array();
$labelsWeek = array();
$labelsMonth = array();
$labelsYear = array();


foreach ($balances as $fecha => $balance) {
    if (strtotime($fecha) > strtotime(date("Y-m-d", strtotime('last Monday'))) && strtotime($fecha) < strtotime(date("Y-m-d", strtotime('next Monday')))) {
        $labelsWeek[] = $fecha;
        $dataWeek[] = $balance['total'];
    }
    if (strtotime($fecha) > strtotime(date("Y-m-d", strtotime('first day of this month'))) && strtotime($fecha) < strtotime(date("Y-m-d", strtotime('last day of this month')))) {
        $labelsMonth[] = $fecha;
        $dataMonth[] = $balance['total'];
    }
    if (strtotime($fecha) > strtotime(date("Y-m-d", strtotime('first day of January'))) && strtotime($fecha) < strtotime(date("Y-m-d", strtotime('last day of December')))) {
        $labelsYear[] = $fecha;
        $dataYear[] = $balance['total'];
    }
    $labels[] = $fecha;
    $data[] = $balance['total'];
}

$stmt->close();
$conector->close();

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Balance</title>
    <link rel="stylesheet" href="style.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.9.1/chart.min.js"></script>
    <style>
    </style>
</head>

<body>
    <div class="wrapper">
        <header class="header-main">
            <div class=header-upper>
                <div class=container>
                    <div class=row>
                        <ul>
                            <li><a href='logout.php'> Log Out </a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="header-lower clearfix">
                <div class="container">
                    <div class="row">
                        <div class="menubar">
                            <nav class="navbar">
                                <div class="nav-wrapper">
                                    <div class="nav-menu">
                                        <ul class="nav navbar-nav menu-bar">
                                            <li><a href=balance.php id=viewLink class="active">View<span></span>
                                                    <span></span>
                                                    <span></span> <span></span></a></li>
                                            <li><a href=newMovement.html id=modifyLink>Create Movement<span></span>
                                                    <span></span>
                                                    <span></span>
                                                    <span></span></a></li>
                                            <li><a href=index.php id=removeLink>Edit<span></span> <span></span>
                                                    <span></span> <span></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section class="about">
            <div class=container>
                <div class=row>
                    <div class=about-wrap>
                        <div class=nav-header id=aboutTab>
                            <ul class="nav nav-tabs clearfix" role=tablist>
                                <li><a href=#w aria-controls=w role=tab data-toggle=tab onclick="mostrarContenido('w')">Week</a>
                                </li>
                                <li><a href=#mon aria-controls=mon role=tab data-toggle=tab onclick="mostrarContenido('mon')">Month</a></li>
                                <li><a href=#y aria-controls=y role=tab data-toggle=tab onclick="mostrarContenido('y')">Year</a></li>
                                <li class=active><a href=#all aria-controls=all role=tab data-toggle=tab onclick="mostrarContenido('all')">All</a></li>
                            </ul>
                        </div>
                        <div class="tab-content nav-content">
                            <div role=tabpanel class="tab-pane fade in" id=all>
                                <div class=innerWrapper>
                                    <aside>
                                        <div class="center">
                                            <section class="balance-container">
                                                <div class="balance-chart">
                                                    <canvas id="myChartAll" width="300" height="150"></canvas>
                                                </div>
                                            </section>
                                        </div>
                                    </aside>
                                </div>
                            </div>
                            <div role=tabpanel class="tab-pane active fade in" id=mon>
                                <div class=innerWrapper>
                                    <aside>
                                        <div class="center">
                                            <section class="balance-container">
                                                <div class="balance-chart">
                                                    <canvas id="myChartMonth" width="300" height="150"></canvas>
                                                </div>
                                            </section>
                                        </div>
                                    </aside>
                                </div>
                            </div>

                            <div role=tabpanel class="tab-pane fade in" id=y>
                                <div class=innerWrapper>
                                    <aside>
                                        <div class="center">
                                            <section class="balance-container">
                                                <div class="balance-chart">
                                                    <canvas id="myChartYear" width="300" height="150"></canvas>
                                                </div>
                                            </section>
                                        </div>
                                    </aside>
                                </div>
                            </div>

                            <div role=tabpanel class="tab-pane fade in" id=w>
                                <div class=innerWrapper>
                                    <aside>
                                        <div class="center">
                                            <section class="balance-container">
                                                <div class="balance-chart">
                                                    <canvas id="myChartWeek" width="300" height="150"></canvas>
                                                </div>
                                            </section>
                                        </div>
                                    </aside>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script>
            function mostrarContenido(opcion) {
                var contenidoWeek = document.getElementById('w');
                var contenidoYear = document.getElementById('y');
                var contenidoAll = document.getElementById('all');
                var contenidoMonth = document.getElementById('mon');

                if (opcion === 'w') {
                    contenidoWeek.style.display = 'block';
                    contenidoYear.style.display = 'none';
                    contenidoAll.style.display = 'none';
                    contenidoMonth.style.display = 'none';

                } else if (opcion === 'y') {
                    contenidoWeek.style.display = 'none';
                    contenidoYear.style.display = 'block';
                    contenidoAll.style.display = 'none';
                    contenidoMonth.style.display = 'none';

                } else if (opcion === 'mon') {
                    contenidoWeek.style.display = 'none';
                    contenidoYear.style.display = 'none';
                    contenidoAll.style.display = 'none';
                    contenidoMonth.style.display = 'block';

                } else {
                    contenidoWeek.style.display = 'none';
                    contenidoYear.style.display = 'none';
                    contenidoAll.style.display = 'block';
                    contenidoMonth.style.display = 'none';
                   
                }
                const navTabs = document.querySelector('.nav-tabs'); // Get the .nav-tabs element

                navTabs.querySelectorAll('li').forEach(tab => { // Loop through all tabs
                    tab.classList.remove('active'); // Remove 'active' class from all tabs
                });

                const selectedTab = document.querySelector(`a[href="#${opcion}"]`); // Find the clicked tab
                selectedTab.parentElement.classList.add('active'); // Add 'active' class to the clicked tab's parent (li)

            }
        </script>
        <script>
            const ctxWeek = document.getElementById('myChartWeek').getContext('2d');
            const ctxMonth = document.getElementById('myChartMonth').getContext('2d');
            const ctxYear = document.getElementById('myChartYear').getContext('2d');
            const ctxAll = document.getElementById('myChartAll').getContext('2d');

            const chartWeek = new Chart(ctxWeek, {
                type: 'line',
                data: {
                    labels: <?php echo json_encode($labelsWeek); ?>,
                    datasets: [{
                        label: 'Balance €/day',
                        data: <?php echo json_encode($dataWeek); ?>,
                        backgroundColor: 'lightgreen', // Cambio del color de fondo a negro
                        borderColor: 'lightgreen',
                        lineTension: 0.4, // Ajusta la suavidad de la línea
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });

            const chartMonth = new Chart(ctxMonth, {
                type: 'line',
                data: {
                    labels: <?php echo json_encode($labelsMonth); ?>,
                    datasets: [{
                        label: 'Balance €/day',
                        data: <?php echo json_encode($dataMonth); ?>,
                        backgroundColor: 'lightgreen', // Cambio del color de fondo a negro
                        borderColor: 'lightgreen',
                        lineTension: 0.4, // Ajusta la suavidad de la línea
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });

            const chartYear = new Chart(ctxYear, {
                type: 'line',
                data: {
                    labels: <?php echo json_encode($labelsYear); ?>,
                    datasets: [{
                        label: 'Balance €/day',
                        data: <?php echo json_encode($dataYear); ?>,
                        backgroundColor: 'lightgreen', // Cambio del color de fondo a negro
                        borderColor: 'lightgreen',
                        lineTension: 0.4, // Ajusta la suavidad de la línea
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });

            const chartAll = new Chart(ctxAll, {
                type: 'line',
                data: {
                    labels: <?php echo json_encode($labels); ?>,
                    datasets: [{
                        label: 'Balance €/day',
                        data: <?php echo json_encode($data); ?>,
                        backgroundColor: 'lightgreen', // Cambio del color de fondo a negro
                        borderColor: 'lightgreen',
                        lineTension: 0.4, // Ajusta la suavidad de la línea
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });

        </script>
        <footer class=footer-type02>
            <div class=container>
                <div class=row>
                    <div class=footer-container>
                        <ul>
                            <li>Total income:
                                <?php echo $income ?><span>€</span>
                            </li>
                            <li>Balance:
                                <?php echo $totalBalance ?><span>€</span>
                            </li>
                            <li>Total Expense:
                                <?php echo $expense ?><span>€</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</body>

</html>