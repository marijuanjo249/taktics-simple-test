<?php 
require_once("DbManager.php");
function remove($ids)
{
    $conector = DbManager::crearConector();
    $stmt = $conector->prepare("DELETE FROM balance WHERE idBalance = ?");
    $stmt->bind_param("i", $idd);
    foreach ($ids as $id) {
        $idd = intval($id); // Asegurarse de que el ID es un número entero
        $stmt->execute();
    }
    $stmt->close();
    $conector->close();
}

// Verificar si se han enviado IDs para eliminar
if (isset($_POST['eliminar'])) {
    $idsEliminar = $_POST['eliminar'];
    remove($idsEliminar);
}

header("Location: index.php");