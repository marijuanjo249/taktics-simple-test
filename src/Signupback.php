<?php
require_once("DbManager.php");
session_start();
$usuario = urldecode($_GET["name"]);
$contrasenia = urldecode($_GET["contrasenia"]);
// Generar el hash de la contraseña
$hash_contrasenia = password_hash($contrasenia, PASSWORD_DEFAULT);

$conector = DbManager::crearConector();
$stmt_verificacion = $conector->prepare("SELECT COUNT(*) FROM usuario WHERE usuario = ?");
$stmt_verificacion->bind_param("s", $usuario);
$stmt_verificacion->execute();
$stmt_verificacion->bind_result($cantidad_usuarios);
$stmt_verificacion->fetch();
$stmt_verificacion->close();

if ($cantidad_usuarios > 0 ) {
    $mensaje = "¡Usuario ya escogido!";
    $_SESSION['mensaje_alerta'] = $mensaje;
    header("Location: Signup.php");
    ; // Finalizar la ejecución del script
} else {
    //$stmt->close(); // Cerrar el statement anterior
    // Preparar y ejecutar la consulta de actualización
    $stmt = $conector->prepare("INSERT into usuario SET usuario = ?, contrasenia = ?");
    $stmt->bind_param("ss", $usuario, $hash_contrasenia);
    $stmt->execute();
    $stmt->close();
    $conector->close();
    $mensaje = "¡Actualización realizada con éxito!";
    $_SESSION['mensaje_alerta'] = $mensaje;
    $_SESSION['usuario'] = $usuario;
    header("Location: index.php");
}