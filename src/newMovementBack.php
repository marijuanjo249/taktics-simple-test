<?php
session_start();
require_once("DbManager.php");

$user = $_SESSION["usuario"];
$name = urldecode($_GET["name"]);
$amount = urldecode($_GET["amount"]);
$category = urldecode($_GET["category"]);
$date = urldecode($_GET["date"]);
$type = urldecode($_GET["type"]);

$conector = DbManager::crearConector();
$stmt = $conector->prepare("SELECT idUsuario FROM usuario WHERE usuario = ?");
$stmt->bind_param("s", $user);
$stmt->execute();
$stmt->bind_result($id);
$stmt->fetch();
$stmt->close();

$stmt = $conector->prepare("INSERT INTO balance (idBalance ,tipo, amount, name, category, date, iduser ) VALUES ('', ?, ?, ?, ?, ?, ?)");
$stmt->bind_param("issssi", $type, $amount, $name, $category, $date, $id);
$stmt->execute();
$stmt->close();
$conector->close();

header("Location: balance.php");
