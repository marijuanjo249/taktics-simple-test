<?php
session_start();
require_once("DbManager.php");

$name = urldecode($_GET["name"]);
$id = urldecode($_GET["idBalance"]);
$amount = urldecode($_GET["amount"]);
$category = urldecode($_GET["category"]);

$conector = DbManager::crearConector();

$stmt = $conector->prepare("UPDATE balance SET 'name' = ?, amount = ?, category = ? WHERE idBalance = ?");
$stmt->bind_param("sssi", $name, $amount, $category, $id);
$stmt->execute();
$stmt->close();
$conector->close();
$mensaje = "¡Actualización realizada con éxito!";
$_SESSION['mensaje_alerta'] = $mensaje;
header("Location: index.php");
