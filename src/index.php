<?php
session_start();
require_once("DbManager.php");
if (isset($_SESSION["usuario"])) {
    $usuario = $_SESSION["usuario"];
} else {
    header("Location: iniciarSesion.php");
}

// Variables para el footer
$income = 0;
$expense = 0;
$total = 0;

// Consulta para obtener la clasificación de equipos
$conector = DbManager::crearConector();
$stmt = $conector->prepare("SELECT balance.*, usuario.usuario from balance join usuario on usuario.usuario = ? and usuario.idUsuario=balance.iduser ORDER BY balance.date DESC");
$stmt->bind_param("s", $usuario);
$stmt->execute();
$result = $stmt->get_result();
$balances = $result->fetch_all(MYSQLI_ASSOC);
$totalBalances = $balances;
$stmt->close();
$conector->close();

//Calculo variables para el footer
foreach ($totalBalances as $totalBalance) {
    if ($totalBalance['tipo'] == 0) {
        $income += $totalBalance['amount'];
        $total += $totalBalance['amount'];
    } else {
        $expense += $totalBalance['amount'];
        $total -= $totalBalance['amount'];
    }
}
?>

<!doctype html>
<html class=no-js lang="">

<head>
    <meta charset=utf-8>
    <meta name=description content="">
    <meta name="viewport"
        content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <title>Balance</title>
    <link rel=stylesheet href=style.css>
    <script>

        function enviarDatos() {
            document.getElementById("formEnvair").submit();
        }

    </script>
</head>

<body>
    <div class="wrapper">
        <header class="header-main">
            <div class=header-upper>
                <div class=container>
                    <div class=row>
                        <ul>
                            <li><a href='logout.php'> Log Out </a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="header-lower clearfix">
                <div class="container">
                    <div class="row">
                        <div class="menubar">
                            <nav class="navbar">
                                <div class="nav-wrapper">
                                    <div class="nav-menu">
                                        <ul class="nav navbar-nav menu-bar">
                                            <li><a href=balance.php id=viewLink>View<span></span>
                                                    <span></span>
                                                    <span></span> <span></span></a></li>
                                            <li><a href=newMovement.html id=modifyLink>Create Movement<span></span>
                                                    <span></span>
                                                    <span></span>
                                                    <span></span></a></li>
                                            <li><a href=index.php id=removeLink class="active">Edit<span></span>
                                                    <span></span>
                                                    <span></span> <span></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section class="about">
            <div class=container>
                <div class=row>
                    <div class=about-wrap>
                        <div class="tab-content nav-content">
                            <div role=tabpanel>
                                <h2 class=heading><a style="color:black;">Balance</a></h2>
                                <div class=innerWrapper>
                                    <aside>
                                        <div class="center2">
                                            <form action='removeBack.php' id="formEnvair" method="post">
                                                <ul class="ticketInfo">
                                                    <?php                                                    
                                                    $pageSize = 5; // Number of results per page
                                                    $currentPage = isset($_GET['page']) ? (int) $_GET['page'] : 1; // Get current page from URL parameter (default 1)
                                                    
                                                    $startIndex = ($currentPage - 1) * $pageSize;
                                                    $endIndex = min($currentPage * $pageSize, count($balances));

                                                    foreach (array_slice($balances, $startIndex, $endIndex) as $balance): ?>                                                        
                                                        <li>
                                                            <ul class="t_info clearfix"
                                                                style="background-color:<?php echo $balance['tipo'] == 0 ? "#B2B2B2" : "#6C6C6C"; ?>">
                                                                <li style="color:white;">                                                                  
                                                                </li>
                                                                <li>
                                                                    <div class="headline01 clearfix">
                                                                        <span>
                                                                            <?php echo $balance['name']; ?>
                                                                        </span>
                                                                        <span class="vs">
                                                                            <?php echo $balance['tipo'] == 0 ? $balance['amount'] . "€" : "- " . $balance['amount'] . "€"; ?>
                                                                        </span>
                                                                    </div>
                                                                    <div class="ticketInner_info paragraph02 clearfix">
                                                                        <span>
                                                                            <?php echo $balance['date']; ?>
                                                                        </span>
                                                                        <span>
                                                                            <?php echo $balance['category']; ?>
                                                                        </span>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <a href="procesar.php?m=<?php echo $balance['idBalance']; ?>"
                                                                        id="botonEditar"
                                                                        class="btn-small01 btn-white">Edit</a>
                                                                </li>
                                                                <li>
                                                                    <input type="checkbox" class="" name="eliminar[]"
                                                                        value="<?php echo $balance['idBalance']; ?>">
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>

                                                <?php
                                                // Display pagination links only if there are more pages
                                                if (count($balances) > $pageSize) {
                                                    $totalPages = ceil(count($balances) / $pageSize);
                                                    echo '<ul class="pagination">';

                                                    // Previous page link (disabled if on first page)
                                                    if ($currentPage > 1) {
                                                        echo '<li><a  class="btn-small01 btn-white center" href="?page=' . ($currentPage - 1) . '">« Previous</a></li>';
                                                    } else {
                                                        echo '<li class="disabled"><a  class="btn-small01 btn-white" href="#">« Previous</a></li>';
                                                    }

                                                    //Page number links
                                                    for ($i = 1; $i <= $totalPages; $i++) {
                                                        $activeClass = ($currentPage == $i) ? ' class="active"' : '';
                                                        echo '<li' . $activeClass . '><a  class="btn-small01 btn-white" href="?page=' . $i . '">' . $i . '</a></li>';
                                                    }

                                                    // Next page link (disabled if on last page)
                                                    if ($currentPage < $totalPages) {
                                                        echo '<li><a  class="btn-small01 btn-white center" href="?page=' . ($currentPage + 1) . '">Next »</a></li>';
                                                    } else {
                                                        echo '<li class="disabled"><a  class="btn-small01 btn-white" href="#">Next »</a></li>';
                                                    }

                                                    echo '</ul>';
                                                }
                                                ?>

                                                <button type="submit" class="btn btn-white" id="botonEliminar"
                                                    onclick="enviarDatos()">Remove Selection</button>
                                            </form>
                                        </div>
                                    </aside>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer class=footer-type02>
            <div class=container>
                <div class=row>
                    <div class=footer-container>
                        <ul>
                            <li>Total income:
                                <?php echo $income ?><span>€</span>
                            </li>
                            <li>Balance:
                                <?php echo $total ?><span>€</span>
                            </li>
                            <li>Total Expense:
                                <?php echo $expense ?><span>€</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</body>

</html>