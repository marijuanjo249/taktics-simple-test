# taktics-simple-test

## Prerequisites
The solution must be retrieved with the technology you are used to.

The solution must include execution instructions.

If the reviewer can't execute the solution, it will count as failed.

## Test requirements
We want a webpage with basic session handling:

- An user must be able to login with username and password
- If the user doesn't exists, should show an error to the user
- If the login is correct, the page should navigate to a balance page

The balance page will show a list of incomes and expenses with the following data:
  
- A type, can be an income or an expense
- A name
- A date
- An amount
- A category

Then on the footer will show 3 totals:

- Income totals that will be the sum of all incomes
- Expense totals that will be the sum of all expenses
- Balance that will be the sum of incomes minus the sum of expenses

The balance page has the following actions:

- Create a new movement that can be an expense or an income and will have as a form all the field aforementioned.
- Possibility to edit some movement
- Possibility to remove one or multiple movements

The data handled on the page must be saved on a database.

## Delivering instructions
Create a repo of your choice with the solution and invite jplaza@taktics.net to the repository and the minimal permissions to download or clone the repository.